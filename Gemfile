source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'pg'
gem 'puma', '~> 3.7'
gem 'rails', '~> 5.1.1'
gem 'bootstrap-sass', '~> 3.3.7'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.47'
gem 'bootswatch-rails', '~> 3.3.5'
gem 'coffee-rails', '~> 4.2'
gem 'font-awesome-rails'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails', '~> 4.3.1'
gem 'jquery-ui-rails', '~> 6.0.1', github: 'jquery-ui-rails/jquery-ui-rails'
gem 'momentjs-rails', '>= 2.9.0'
gem 'sass-rails', '~> 5.0'
gem 'slim-rails', '~> 3.1.2'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
gem 'bcrypt', '~> 3.1.7'
gem 'devise', github: 'plataformatec/devise'
gem 'devise-token_authenticatable'
gem 'devise_token_auth'
gem 'paperclip', '~> 5.1.0'
gem 'pundit'
gem 'select2-rails', '~> 4.0.3'
gem 'will_paginate', '~> 3.1.0'
gem 'responders'
gem 'state_machine'
gem 'storext', '~>2.2.2'
gem 'interactor-rails'
### API ###
gem 'versionist'
gem 'active_model_serializers'

# Run Server
gem 'foreman'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %I[mri mingw x64_mingw]
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'brakeman', require: false
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'meta_request'
  gem 'pry'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'pry-stack_explorer'
  gem 'rack-mini-profiler', '~> 0.10.2'
  gem 'rubocop', '~> 0.49.1'
  gem 'rubocop-rspec', '~> 1.15.1'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
  gem 'factory_girl_rails', '~> 4.8.0', '>=4.8.0', require: false
  gem 'ffaker'
end

group :test do
  gem 'capybara'
  gem 'cucumber-rails', require: false
  gem 'database_cleaner'
  gem 'factory_girl_rails', '~> 4.8.0', '>=4.8.0', require: false
  # gem 'json_spec'
  gem 'ffaker'
  gem 'poltergeist', '~> 1.9.0'
  gem 'rails-controller-testing'
  gem 'rspec-rails', '~> 3.6.0', '>=3.6.0'
  gem 'shoulda-matchers', git: 'https://github.com/thoughtbot/shoulda-matchers.git', branch: 'rails-5'
  gem 'pundit-matchers', '~> 1.3.1'
  gem 'simplecov', '~> 0.15.0', require: false
end

gem 'rails_12factor', group: :production
