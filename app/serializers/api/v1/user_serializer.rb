module Api
  module V1
    class UserSerializer < API::ApplicationSerializer
      attributes :email, :authentication_token
    end
  end
end
