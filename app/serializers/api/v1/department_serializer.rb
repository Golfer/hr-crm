module Api
  module V1
    class DepartmentSerializer < API::ApplicationSerializer
      attributes :id, :name, :description, :count_of_employees
      has_many :employees, include: true
    end
  end
end
