module Api
  module V1
    class EmployeeSerializer < API::ApplicationSerializer
      attributes :id, :email, :in_vocation
      has_one :profile
    end
  end
end
