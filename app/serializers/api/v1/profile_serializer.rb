module Api
  module V1
    class ProfileSerializer < API::ApplicationSerializer
      attributes :full_name, :first_name, :last_name, :date_birth, :gender, :working_hours
    end
  end
end
