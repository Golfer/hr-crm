module Api
  module V2
    class ProfileSerializer < API::ApplicationSerializer
      attributes :full_name, :date_birth, :gender
    end
  end
end
