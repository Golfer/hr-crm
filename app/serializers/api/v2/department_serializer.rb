module Api
  module V2
    class DepartmentSerializer < API::ApplicationSerializer
      attributes :id, :name, :count_of_employees
      has_many :employees, include: true
    end
  end
end
