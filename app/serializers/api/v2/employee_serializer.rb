module Api
  module V2
    class EmployeeSerializer < API::ApplicationSerializer
      attributes :email
      has_one :profile
      has_many :departments, include: true
    end
  end
end
