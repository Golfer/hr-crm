$ ->
  $(document).on("ajax:error", (xhr, status, error) ->
    console.log($(this).append(xhr.responseText))
    console.log error
  )

  $('body').on('focus', '.date_picker input', (event) ->
    event.preventDefault()
    $(this).datetimepicker(
      viewMode: 'years'
      format: 'YYYY-MM-DD'
      showTodayButton: true
      maxDate: new Date
    )
    return
  ).on('focus', '.time_picker input', (event) ->
    event.preventDefault()
    $(this).datetimepicker(
      format: 'LT'
      showClear: true
    )
    return
  ).on('click', '.connect_employees', (event) ->
    event.preventDefault()
    $('#employeeesModal').modal 'show'
  ).on('click', '.related_to_departments', (event) ->
    event.preventDefault()
    $('#departmentsModal').modal 'show'
  )

