class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include Pundit

  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

  before_action :configure_permitted_parameters, if: :devise_controller?

  PER_PAGE = 10

  def render_unprocessable_entity_response(exception)
    flash.now[:error] = exception.record.errors
    render action: :index, status: :unprocessable_entity
  end

  def render_not_found_response(exception)
    flash.now[:error] = exception.message
    render action: :index, status: :unprocessable_entity
  end

  def render_error_response(exception)
    flash.now[:error] = exception.message
    render action: :index, status: exception.http_status
  end

  def sort_params(sort)
    return unless sort || sort.blank?
    "#{order_by(sort).squish} #{order_for(sort)}"
  end

  def order_by(order_by)
    if order_by && order_by.blank? || order_by.nil? || order_by == '-'
      'id'
    else
      order_by[0] == '-' ? order_by[1..-1] : order_by
    end
  end

  def order_for(order_for)
    order_for && order_for[0] == '-' ? 'DESC' : 'ASC'
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end
end
