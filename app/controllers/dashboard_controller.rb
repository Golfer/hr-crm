class DashboardController < ApplicationController
  before_action :fetch_departments, only: %I[index]
  def index; end

  private
  def fetch_departments
    @departments ||=
      Department.eager_load(employees: :profile)
        .paginate(page: params[:page], per_page: 3)
  end
end
