module Api
  module V2
    class EmployeesController < Api::EmployeesController
      def pagination_links(data_object, sort:)
        super
        list = {}
        list[:current_page] = api_v2_employees_url(page: data_object.current_page, sort: sort)
        list[:prev_page] = api_v2_employees_url(page: data_object.previous_page, sort: sort) unless data_object.previous_page.nil?
        list[:next_page] = api_v2_employees_url(page: data_object.next_page, sort: sort) unless data_object.next_page.nil?
        list
      end
    end
  end
end
