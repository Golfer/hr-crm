module Api
  module V1
    class DepartmentsController < Api::DepartmentsController
      def pagination_links(data_object, sort:)
        super
        list = {}
        list[:current_page] = api_v1_departments_url(page: data_object.current_page, sort: sort)
        list[:first_page] = api_v1_departments_url(page: 1, sort: sort) unless data_object.current_page == 1
        list[:prev_page] = api_v1_departments_url(page: data_object.previous_page, sort: sort) unless data_object.previous_page.nil?
        list[:next_page] = api_v1_departments_url(page: data_object.next_page, sort: sort) unless data_object.next_page.nil?
        list[:last_page] = api_v1_departments_url(page: data_object.total_pages, sort: sort) unless data_object.total_pages == data_object.next_page
        list
      end
    end
  end
end
