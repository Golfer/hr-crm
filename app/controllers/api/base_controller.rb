module Api
  class BaseController < ActionController::API
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

    include Rails.application.routes.url_helpers
    include Api::BaseModule

    respond_to :json

    before_action :authenticate_user!, only: %I[show create update destroy]
    before_action :current_object, only: %I[show update destroy]

    attr_accessor :current_object, :class_params, :object_created

    PER_PAGE = 5

    def authenticate_user!
      return if current_user
      respond_with({ error: 'You are not authorized' }, status: 401)
    end

    def api_version_number
      "V#{fetch_api_number.nil? ? 1 : fetch_api_number.to_i}"
    end

    def set_namespace_for_serializer
      self.namespace_for_serializer = API::send(api_version_number)
    end

    def fetch_api_number
      request.original_fullpath.split('?').first[/\d+/]
    end

    def show
      respond_with current_object, status: :ok
    end

    def create
      if object_created.save
        respond_with controller_name.classify.constantize.last, status: :created
      else
        respond_with object_created, status: :unprocessable_entity
      end
    end

    def update
      if current_object.update!(class_params)
        render json: current_object, status: :ok
      else
        respond_with current_object.errors, status: :unprocessable_entity
      end
    end

    def destroy
      current_object.destroy ? head(204) :  head(:unprocessable_entity)
    end

    def render_unprocessable_entity_response(exception)
      render json: exception.record.errors, status: :unprocessable_entity
    end

    def render_not_found_response(exception)
      render json: { error: exception.message }, status: :not_found
    end

    def render_error_response(exception)
      render json: { message: exception.message, code: exception.code }, status: exception.http_status
    end

    def pagination_meta(object_data)
      {
        pagination:  {
          current_page: object_data.current_page,
          per_page: object_data.limit_value,
          total_pages: object_data.total_pages,
          total_entries: object_data.total_entries
        }
      }
    end

    def sort_params(sort)
      return unless sort || sort.blank?
      "#{order_by(sort).squish} #{order_for(sort)}"
    end

    def order_by(order_by)
      if order_by && order_by.blank? || order_by.nil? || order_by == '-'
        ''
      else
        order_by[0] == '-' ? order_by[1..-1] : order_by
      end
    end

    def order_for(order_for)
      order_for && order_for[0] == '-' ? 'DESC' : 'ASC'
    end

    private

    def current_object
      controller_name.classify.constantize.find(params[:id])
    end

    def class_params
      send("#{controller_name.classify.downcase}_params".to_sym)
    end

    def object_created
      controller_name.classify.constantize.create class_params
    end
  end
end
