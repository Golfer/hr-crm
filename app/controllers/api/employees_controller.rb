module Api
  class EmployeesController < Api::BaseController
    before_action :load_employees, only: :index
    before_action :current_object, only: %i[show update destroy]
    attr_accessor :employees, :current_object

    before_action :authenticate_user!, except: %I[index]

    def default_serializer_options
      { serializer: namespace_for_serializer::EmployeeSerializer }
    end

    def index
      respond_with employees, meta: pagination_meta(employees).merge!(links: pagination_links(employees, sort: params[:sort])), adapter: :json
    end

    def create
      result = EmployeeInteractors::Create.call(employee_params: employee_params, profile_params: profile_params)
        if result.employee.errors.present? || result.employee.profile.errors.present?
          data = {}
          data[:employee] = result.employee.errors.full_messages if result.employee.errors.present?
          data[:profile] = result.employee.profile.errors.full_messages if result.employee.profile.errors.present?
          render json: data, status: :unprocessable_entity
        else
          respond_with result.employee, status: :created
        end
    end

    def update
      result = EmployeeInteractors::Update.call(current_object: current_object, employee_params: employee_params, profile_params: profile_params)
      if result.employee.errors.present? || result.employee.profile.errors.present?
        data = {}
        data[:employee] = result.employee.errors.full_messages if result.employee.errors.present?
        data[:profile] = result.employee.profile.errors.full_messages if result.employee.profile.errors.present?
        render json: data, status: :unprocessable_entity
      else
        render json: result.employee, status: :ok
      end
    end

    def show
      super
    end

    def destroy
      super
    end

    private

    def employee_params
      params.require(:employee).permit(:id, :vacation, :status, :email)
    end

    def profile_params
      return unless params.require(:employee)[:profile].present?
      params.require(:employee)[:profile].permit(
          :id, :first_name, :last_name, :second_name, :gender, :date_birth,
          :phone_number, :time_zone, :avatar,
          working_hours: [
            { sunday: %I[from to] },
            { monday: %I[from to] },
            { tuesday: %I[from to] },
            { wednesday: %I[from to] },
            { thursday: %I[from to] },
            { friday: %I[from to] },
            { saturday: %I[from to] }
          ]
      )
    end

    def current_object
      @current_object = Employee.find(params[:id])
    end
    
    def order_by(order_by)
      super
      if order_by && order_by.blank? || order_by.nil? || order_by == '-'
        "profiles.first_name"
      else
        order_by[0] == '-' ? "profiles.#{order_by[1..-1]}" : "profiles.#{order_by}"
      end
    end

    def load_employees
      @employees ||=
        Employee.eager_load(:profile)
          .paginate(page: params[:page], per_page: params[:per_page] || Api::BaseController::PER_PAGE)
          .order(sort_params(params[:sort]))
    end

    protected

      def pagination_links(data_object, sort:)
        {
          current_page: '',
          first_page: '',
          next_page: '',
          prev_page: '',
          last_page: ''
        }
      end

  end
end
