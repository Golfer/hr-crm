module Api
  class DepartmentsController < Api::BaseController
    before_action :load_departments, only: :index
    before_action :current_object, only: %i[show update destroy connected_employees connect_employees disconnect_employees]
    attr_accessor :departments
    before_action :authenticate_user!, except: %I[index]

    def default_serializer_options
      { serializer: namespace_for_serializer::DepartmentSerializer }
    end

    def index
      respond_with departments, meta: pagination_meta(departments).merge!(links: pagination_links(departments, sort: params[:sort])), adapter: :json
    end

    def show
      super
    end

    def create
      super
    end

    def update
      super
    end

    def destroy
      super
    end

    def connect_employees
      if department_params[:employee] && department_params[:employee].kind_of?(Array)
        Department.transaction do
          department_params[:employee].each do |employee|
            if employee[:email].present? && !employee[:email].blank?
              employee = Employee.find_by_email(employee[:email])
              next if DepartmentsEmployee.where(employee_id: employee.try(&:id)).present?
              current_object.employees << employee unless employee.nil?
            end
          end
        end
        respond_with current_object, status: :ok
      else
        respond_with current_object, status: :unprocessable_entity
      end
    end

    def disconnect_employees
      if department_params[:employee] && department_params[:employee].is_a?(Array)
        Department.transaction do
          department_params[:employee].each do |employee|
            if employee[:email].present? && !employee[:email].blank?
              employee = Employee.find_by_email(employee[:email])
              DepartmentsEmployee.where(employee_id: employee.id, department_id: current_object.id).delete_all
            end
          end
        end
        respond_with current_object, status: :ok
      else
        respond_with current_object, status: :unprocessable_entity
      end
    end

    def order_by(order_by)
      super
      if order_by && order_by.blank? || order_by.nil? || order_by == '-'
        'name'
      else
        order_by[0] == '-' ? order_by[1..-1] : order_by
      end
    end

    private

    def load_departments
      @departments ||= Department
                         .paginate(page: params[:page], per_page: Api::BaseController::PER_PAGE)
                         .order(sort_params(params[:sort]))
    end

    def department_params
      params.require(:department).permit(:id, :name, :description, employee: [:id, :email, :first_name, :last_name])
    end

    protected

    def pagination_links(data_object, sort:)
      {
        current_page: '',
        first_page: '',
        next_page: '',
        prev_page: '',
        last_page: ''
      }
    end
  end
end
