module Api
  class SessionsController < Api::BaseController
    skip_before_action :authenticate_user!
    skip_before_action :current_object

    def create
      resource = User.find_for_database_authentication(email: params[:user][:email])
      return invalid_login_attempt unless resource
      if resource&.valid_password?(params[:user][:auth_password])
        sign_in(:user, resource)
        resource.ensure_authentication_token!
        render json: resource.as_json(only: [:email, :authentication_token]), status: :created
      else
        warden.custom_failure!
        head(:unauthorized)
      end
    end

    def destroy
      resource = if params[:id].present?
                   User.find_for_database_authentication(authentication_token: params[:id])
                 else
                   current_user
                 end
      sign_out(resource)
      head(:unauthorized)
    end


    def current
      respond_with current_user, serializer: namespace_for_serializer::UserSerializer
    end

    protected

    def ensure_params_exist
      return unless params[:email].blank? || params[:auth_password].blank?
      render json: { message: 'missing user params for authentification' }, status: 422
    end

    def invalid_login_attempt
      render json: { message: 'Error with your Email or token' }, status: 401
    end
  end
end
