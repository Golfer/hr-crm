class DepartmentsController < ApplicationController
  before_action :authenticate_user!, except: %I[index]
  before_action :departments
  before_action :department, only: %i[show update edit destroy connect_employee connect_employees]
  respond_to :html, :json, :js

  attr_reader :departments, :department

  def new
    @department = Department.new
  end

  def create
    @department = Department.new(department_params)
    if @department.save
      redirect_to departments_path, notice: 'Department create!'
    else
      render :new
    end
  end

  def update
    if department.update(department_params)
      redirect_to department_path(department), notice: 'Department successfully updated!'
    else
      render :edit
    end
  end

  def destroy
    department.destroy
    respond_to do |format|
      format.html { redirect_to departments_path, notice: 'Department was successfully destroyed.' }
      format.js
    end
  end

  def connect_employees
    @employees ||= @department.employees
  end

  def connect_employee
    DepartmentsEmployee.where(department_id: department.id).delete_all
    manipulation_employees params[:department][:employees]
    department_employees
  end

  private

  def departments
    @departments ||= Department.order_by_name.paginate(page: params[:page], per_page: ApplicationController::PER_PAGE)
  end

  def department_employees
    @employees ||= department.employees
  end

  def department_params
    params.require(:department).permit :id, :name, :description
  end

  def department
    @department ||= Department.find params[:id]
  end

  def manipulation_employees(employees)
    return if employees.nil?
    employees = employees.reject! { |t| t == '' }
    employees.each do |email|
      @department.employees << Employee.where(email: email)
    end
  end
end
