class EmployeesController < ApplicationController
  before_action :authenticate_user!, except: %I[index]

  before_action :current_employee, only: %I[new create show update edit destroy related_to_department]

  before_action :departments, only: %I[related_to_department]
  before_action :department, only: %I[related_to_department]
  before_action :employees, only: %I[index]

  respond_to :html, :json, :js

  attr_reader :department, :employee

  def index; end

  def new
    @employee = Employee.new
    @employee.build_profile
  end

  def create
    Employee.transaction do
      @employee = Employee.new employee_params
      Profile.transaction do
        @profile = @employee.build_profile profile_params
        if @employee.valid? && @profile.valid? && @employee.save!
          redirect_to employees_path, notice: 'Employee create!'
        else
          render :new
        end
      end
    end
  end

  def edit; end

  def update
    Employee.transaction do
      Profile.transaction do
        if current_employee.update_attributes(employee_params) && current_employee.profile.update_attributes(profile_params)
          redirect_to employees_path, notice: 'Employee update!'
        else
          render :edit
        end
      end
    end
  end

  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_path, notice: 'Employee was successfully destroyed.' }
      format.js
    end
  end

  def related_to_department
    if !department.blank? && params[:connected] == 'true'
      employee.departments << department
    elsif !department.blank? && params[:connected] == 'false'
      employee.departments.delete(department)
    end
  end

  private

  def employee_params
    params.require(:employee).permit(:id, :vacation, :status, :email)
  end

  def profile_params
    return unless params.require(:employee)[:profile_attributes].present?
    params.require(:employee)[:profile_attributes].permit(
      :id, :first_name, :last_name, :second_name, :gender, :date_birth,
      :phone_number, :time_zone, :avatar,
      working_hours: [
        { sunday: %I[from to] },
        { monday: %I[from to] },
        { tuesday: %I[from to] },
        { wednesday: %I[from to] },
        { thursday: %I[from to] },
        { friday: %I[from to] },
        { saturday: %I[from to] }
      ])
  end

  def current_employee
    @employee ||= Employee.find(params[:id]) if params[:id]
  end

  def department
    @department ||= Department.where(id: params[:department_id])
  end

  def departments
    @departments ||= Department.select(:id, :name)
  end

  def employees
    @employees ||=
      Employee.eager_load(:profile)
        .paginate(page: params[:page], per_page: params[:per_page] || ApplicationController::PER_PAGE)
        .order(sort_params(params[:sort]))
  end

  def order_by(order_by)
    super
    if order_by && order_by.blank? || order_by.nil? || order_by == '-'
      'profiles.first_name'
    else
      order_by[0] == '-' ? "profiles.#{order_by[1..-1]}" : "profiles.#{order_by}"
    end
  end
end
