class UsersController < ApplicationController
  before_action :authenticate_user!

  def profile

  end

  def index; end

  def show; end

  def new; end

  def create; end

  def update; end

  def destroy; end
end
