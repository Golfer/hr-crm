module ApplicationHelper
  def twitterized_type(type)
    case type.to_sym
      when :alert
        'warning'
      when :error
        'danger'
      when :notice
        'info'
      when :success
        'success'
      else
        type.to_s
    end
  end

  def error_message_for(field_name, obj_name)
    content_tag :span, obj_name.errors.full_messages_for(field_name.to_sym).first, class: 'help-block' if obj_name.errors.include? field_name
  end

  def all_employees
    @employees ||= Employee.all.select(:id, :email)
  end
end
