module EmployeeInteractors
  class Create
    include Interactor

    def call
      Employee.transaction do
        @employee = Employee.new context.employee_params
        @employee.save
        Profile.transaction do
          @profile = @employee.build_profile context.profile_params
          unless @profile.save
            context.employee = @employee
            context.profile = @profile
            context.fail!
          end
        end
      end
      context.employee = @employee
    end
  end
end
