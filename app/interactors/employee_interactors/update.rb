module EmployeeInteractors
  class Update
    include Interactor

    def call
      current_object = context.current_object
      employee_params = context.employee_params
      profile_params = context.profile_params
      Employee.transaction do
        Profile.transaction do
          current_object.update!(employee_params) if employee_params.present?
          current_object.profile.update!(profile_params) if profile_params.present?
        end
      end
      context.employee = current_object.reload
    end
  end
end
