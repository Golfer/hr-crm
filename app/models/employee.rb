class Employee < ApplicationRecord
  has_many :departments_employees, dependent: :destroy
  has_and_belongs_to_many :departments, -> { distinct }, through: :departments_employees
  has_one :profile
  validates :email, presence: true, length: { maximum: 64 }
  validates :email, uniqueness: true
  validates_format_of :email, with: /@/, length: { maximum: 128 }

  accepts_nested_attributes_for :profile
end
