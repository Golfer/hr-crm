class Profile < ApplicationRecord
  store :settings, accessors: %I[working_hours]
  belongs_to :employee, optional: true
  belongs_to :user, optional: true
  has_attached_file :avatar,
                    styles: { content: '900x300', thumb: '150x50' },
                    default_url: 'profiles/avatar_missing.png'
  validates_attachment_content_type :avatar,
                                    content_type: %w[image/jpg image/jpeg image/png image/gif]
  validates_attachment_size :avatar, less_than: 2.megabytes

  validates :first_name, :last_name, presence: true, length: { minimum: 2 }
  validates :gender, :date_birth, presence: true

  validate :check_working_hours

  def full_name
    "#{first_name} #{last_name}"
  end

  def working_hours_field(*args)
    args.inject(working_hours) { |result, arg| result.try(:[], arg) }
  end

  def check_working_hours
    Date::DAYNAMES.map(&:downcase).each do |day|
      next if working_hours_field(day, :from).blank? && working_hours_field(day, :to).blank?
      errors.add("working_hours_#{day}".to_sym, "Working hours #{day} from #{I18n.translate('errors.messages.blank')}") if working_hours_field(day, :from).blank?
      errors.add("working_hours_#{day}".to_sym, "Working hours #{day} to #{I18n.translate('errors.messages.blank')}") if working_hours_field(day, :to).blank?
      to = working_hours_field(day, :to).is_a?(Time) ? working_hours_field(day, :to) : Time.zone.try(:parse, working_hours_field(day, :to)) rescue nil
      from = working_hours_field(day, :from).is_a?(Time) ? working_hours_field(day, :from) : Time.zone.try(:parse, working_hours_field(day, :from)) rescue nil
      errors.add("working_hours_#{day}".to_sym, "Working hours #{day} from #{I18n.translate('errors.messages.invalid')}") if working_hours_field(day, :from).present? && from.blank?
      errors.add("working_hours_#{day}".to_sym, "Working hours #{day} to #{I18n.translate('errors.messages.invalid')}") if working_hours_field(day, :to).present? && to.blank?
      errors.add("working_hours_#{day}".to_sym, "Working hours #{day} to #{I18n.translate('errors.messages.greater_than', count: from)}") if to.present? && from.present? && to < from
    end
  end
end
