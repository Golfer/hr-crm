class Role < ApplicationRecord
  has_many :roles_users, dependent: :destroy
  has_and_belongs_to_many :users, -> { distinct }, through: :roles_users

  validates :name, presence: true, length: { maximum: 32 }
  validates :name, uniqueness: true

  DEFINED_ROLES = %w[ROLE_BOSS ROLE_HR ROLE_DEV ROLE_MANAGER ROLE_ACCOUNT_MANAGER ROLE_MODERATOR ROLE_EMPLOYEE].freeze
end
