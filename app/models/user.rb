class User < ApplicationRecord
  has_many :roles_users, dependent: :destroy
  has_and_belongs_to_many :roles, -> { distinct }, through: :roles_users

  has_one :profile
  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
         :trackable, :timeoutable, :validatable, :token_authenticatable

  attr_accessor :role, :department, :current_password, :new_password, :confirm_password, :roles_array
  cattr_accessor :current_user

  validates :email, presence: true, length: { maximum: 64 }
  validates :email, uniqueness: true
  validates_format_of :email, with: /@/
  validates :encrypted_password, presence: true, length: { maximum: 128 }

  before_validation do
    email.downcase if email.present?
  end

  after_create do
    roles << Role.find_by_name(role) if role.present? && roles.empty?
    # self.departments << Department.find(self.department) if self.department.present? && self.departments.empty?
  end

  after_update do
    # self.departments << Department.find(self.department) if self.department.present?
    # self.roles << Role.find_by_name(self.role) if self.role.present?
  end

  scope :users_with_role, ->(role) { joins(:roles).where(roles: { name: role }) }
  scope :boss, -> { users_with_role('ROLE_BOSS') }
  scope :hr, -> { users_with_role('ROLE_DEV') }
  scope :dev, -> { users_with_role('ROLE_DEV') }
  scope :manager, -> { users_with_role('ROLE_MANAGER') }
  scope :account_manager, -> { users_with_role('ROLE_ACCOUNT_MANAGER') }
  scope :moderator, -> { users_with_role('ROLE_MODERATOR') }
  scope :employee, -> { users_with_role('ROLE_EMPLOYEE') }

  # FOR checking user role
  def role?(role)
    roles.map(&:name).include?("ROLE_#{role.to_s.upcase}")
  end

  def roles?(role)
    roles.map(&:name).include? role
  end

  {
    boss?: 'ROLE_BOSS',
    hr?: 'ROLE_HR',
    dev?: 'ROLE_DEV',
    employee?: 'ROLE_EMPLOYEE',
    manager?: 'ROLE_MANAGER',
    account_manager?: 'ROLE_ACCOUNT_MANAGER',
    moderator?: 'ROLE_MODERATOR'
  }.each { |name, args| define_method(name) { roles?(args) } }
end
