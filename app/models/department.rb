class Department < ApplicationRecord
  has_many :departments_employees
  has_and_belongs_to_many :employees, -> { distinct }, through: :departments_employees, dependent: :destroy

  validates :name, presence: true, length: { maximum: 64, minimum: 2 }
  validates :name, uniqueness: true

  scope :order_by_name, -> { order(name: :asc) }

  def count_of_employees
    employees.count
  end

  def pagination_links
    { first_page: '',
      next_page: '',
      prev_page: '',
      last_page: '' }
  end
end
