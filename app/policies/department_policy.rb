class DepartmentPolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    current_user.present? && (current_user.boss? || current_user.hr? || current_user.moderator?)
  end

  def create?
    current_user.present? && (current_user.boss? || current_user.hr? || current_user.moderator?)
  end

  def update?
    current_user.present? && (current_user.boss? || current_user.hr? || current_user.moderator?)
  end

  def destroy?
    current_user.present? && (current_user.boss? || current_user.hr? || current_user.moderator?)
  end

  def connect_to_department?
    current_user.present? && (current_user.employee? || current_user.hr? )
  end

  def connect_employees?
    current_user.present? && (current_user.moderator? || current_user.boss? || current_user.hr?)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
