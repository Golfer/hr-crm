class UserPolicy < ApplicationPolicy
  def create_employee_account?
    current_user.employee? && !Employee.where(email: current_user.email).present?
  end
end
