class EmployeePolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    current_user.present?
  end

  def new?
    current_user.present? && (current_user.boss? || current_user.hr? || current_user.moderator?)
  end

  def create?
    current_user.present? && (current_user.boss? || current_user.hr? || current_user.moderator?)
  end

  def update?
    current_user.present? && (current_user.boss? || current_user.hr? || current_user.moderator? || (current_user.email == record.email))
  end

  def destroy?
    current_user.present? && (current_user.boss? || current_user.hr? || current_user.moderator? || (current_user.email == record.email))
  end

  def view_department?
    current_user.present?
  end

  def related_to_department?
    current_user.present? && (current_user.boss? || current_user.moderator? || current_user.hr? || (current_user.email == record.email))
  end

  def create_employee_account?
    current_user.present? && (current_user.moderator? || current_user.hr? || (current_user.email == record.email))
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
