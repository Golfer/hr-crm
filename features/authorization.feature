@javascript
Feature: working with Authorization scenario through UI

  Scenario: User can login to HRM system & view
    When I go to login page
      And I fill-in authorization fields
      And click btn "login"
    Then I should see message "You LOGINED!"

  Scenario: User can't login to HRM system because there is no registred at system
    When I go to login page
    And I fill-in login "email" - "faild@mail.com"
    And I fill-in login "password" - "123123123"
    And click btn "login"
    Then I should see not message "You LOGINED!"
      And I should see message "Invalid email or password"
