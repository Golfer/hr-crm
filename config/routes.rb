require 'versionist/routing_params.rb'

Rails.application.routes.draw do
  root to: 'dashboard#index'

  resources :employees do
    member do
      match :related_to_department, to: 'employees#related_to_department', as: 'related_to_department', via: %i(get)
    end
  end

  resources :departments do
    member do
      match :connect_employee, to: 'departments#connect_employee', as: 'connect_employee', via: %i(get patch)
      match :connect_employees, via: %i(get post)
    end
  end
  # resources :users, only: %I[profile]

  devise_for :users,
             controllers: { sessions: 'users/sessions',
                            registrations: 'users/registrations' }


  ### API ###
  namespace :api, defaults: { format: :json } do
    api_version(api_version_params(1, defaults: { format: :json }, default: true)) do
      resources :departments, only: %i(index show create update destroy) do
        member do
          match :connect_employees, to: 'departments#connect_employees', via: %i(post)
          match :disconnect_employees, to: 'departments#disconnect_employees', via: %i(post)
        end
      end
      resources :employees, only: %i(index show create update destroy)
      resources :sessions, only: %i(create destroy) do
        collection do
          match :current, to: 'sessions#current', via: %i(get)
        end
      end

    end

    api_version(api_version_params(2, defaults: { format: :json })) do
      resources :departments, only: %i(index show create update destroy) do
        member do
          match :connect_employees, to: 'departments#connect_employees', via: %i(post)
          match :disconnect_employees, to: 'departments#disconnect_employees', via: %i(post)
        end
      end
      resources :employees, only: %i(index show create update destroy)
      resources :sessions, only: %i(create destroy) do
        collection do
          match :current, to: 'sessions#current', via: %i(get)
        end
      end
    end

  end
end
