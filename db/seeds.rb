puts "Start Seed!#{Time.now}"
%W[PHP RUBY JAVA AngularJs C C# C++ Delphi Bash Elixir Go Haskell JavaScript Scala].each do |department|
  Department.create name: department
end
time_start = Time.now
1000.times do |i|
  time_fake = DateTime.now().strftime('%Y-%m-%d %H-%M')
  employee = Employee.create email: "#{i}_fake_email#{time_fake}@mail.com"
  employee.create_profile first_name: "#{i}-F_name", last_name: "#{i}-L_name", gender: :male, date_birth: time_fake
  department = Department.order("RANDOM()").first
  department.employees << employee
end
time_finish = Time.now
result_time = time_finish - time_start
puts result_time.round(2)