class AddFieldsToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :first_name, :string
    add_column :employees, :second_name, :string
    add_column :employees, :last_name, :string
    add_column :employees, :gender, :string
    add_column :employees, :phone_number, :string
    add_column :employees, :date_birth, :string
    add_column :employees, :time_zone, :string
  end

  add_attachment :employees, :employee_avatar
end