class ChangeForExistTables < ActiveRecord::Migration[5.1]
  def change
    remove_column  :users, :first_name
    remove_column  :users, :last_name

    add_column :users, :authentication_token, :string

    drop_table :departments_users
  end
end
