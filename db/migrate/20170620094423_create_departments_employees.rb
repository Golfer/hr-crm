class CreateDepartmentsEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :departments_employees do |t|
      t.references :department, index: true, null: false
      t.references :employee, index: true, null: false
    end

    add_index :departments_employees, %i(department_id employee_id), unique: true
  end
end
