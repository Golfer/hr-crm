class AddRoles < ActiveRecord::Migration[5.1]
  def self.up
    Role.create(name: 'ROLE_BOSS').users
    Role.create(name: 'ROLE_HR').users
    Role.create(name: 'ROLE_DEV').users
    Role.create(name: 'ROLE_MANAGER').users
    Role.create(name: 'ROLE_ACCOUNT_MANAGER').users
    Role.create(name: 'ROLE_MODERATOR').users
    Role.create(name: 'ROLE_EMPLOYEE').users
  end

  def self.down
    Role.find_by_name('ROLE_BOSS').delete
    Role.find_by_name('ROLE_HR').delete
    Role.find_by_name('ROLE_DEV').delete
    Role.find_by_name('ROLE_MANAGER').delete
    Role.find_by_name('ROLE_ACCOUNT_MANAGER').delete
    Role.find_by_name('ROLE_MODERATOR').delete
    Role.find_by_name('ROLE_EMPLOYEE').delete
  end
end
