class MadeChangesProfilesEmployees < ActiveRecord::Migration[5.1]
  def change
    remove_column :employees, :first_name
    remove_column :employees, :second_name
    remove_column :employees, :last_name
    remove_column :employees, :gender
    remove_column :employees, :phone_number
    remove_column :employees, :date_birth
    remove_column :employees, :time_zone
    rename_column :employees, :vacation, :in_vocation
    change_column :employees, :in_vocation, :boolean, default: false
    remove_attachment :employees, :employee_avatar

    add_reference :profiles, :user, index: true, null: true
    add_reference :profiles, :employee, index: true, null: true
    remove_column :profiles, :profiletable_type
    remove_column :profiles, :profiletable_id
    remove_column :profiles, :settings
    add_column :profiles, :settings, :jsonb

    add_attachment :departments, :logo

    add_index :employees, %i(email in_vocation), unique: true
  end
end
