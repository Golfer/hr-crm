class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :second_name
      t.string :last_name
      t.string :gender
      t.string :phone_number
      t.string :date_birth
      t.string :time_zone
      t.binary :settings
      t.boolean :status
      t.attachment :avatar

      t.string :profiletable_type
      t.integer :profiletable_id

      t.timestamps
    end

    add_index :profiles, [:profiletable_type, :profiletable_id]
  end
end
