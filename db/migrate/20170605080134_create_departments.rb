class CreateDepartments < ActiveRecord::Migration[5.1]
  def up
    create_table :departments do |t|
      t.string :name,        unique: true, null: false
      t.string :description

      t.timestamps
    end
    add_index :departments, :name, unique: true
  end

  def down
    drop_table :departments
  end
end
