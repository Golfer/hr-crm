class CreateDepartmentsUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :departments_users do |t|
      t.references :department, index: true, null: false
      t.references :user, index: true, null: false
    end

    add_index :departments_users, %i(department_id user_id), unique: true
  end
end
