class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :email
      t.boolean :vacation, default: false
      t.boolean :status
      t.binary :settings
    end
  end
end
