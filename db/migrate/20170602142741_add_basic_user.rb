class AddBasicUser < ActiveRecord::Migration[5.1]
  def self.up
    User.create(email: 'boss@mail.com', password: '123123').roles << Role.find_by_name('ROLE_BOSS')
    User.create(email: 'hr@mail.com', password: '123123').roles << Role.find_by_name('ROLE_HR')
    User.create(email: 'dev@mail.com', password: '123123').roles << Role.find_by_name('ROLE_DEV')
    User.create(email: 'manager@mail.com', password: '123123').roles << Role.find_by_name('ROLE_MANAGER')
    User.create(email: 'account_manager@mail.com', password: '123123').roles << Role.find_by_name('ROLE_ACCOUNT_MANAGER')
    User.create(email: 'moderator@mail.com', password: '123123').roles << Role.find_by_name('ROLE_MODERATOR')
  end

  def self.down
    User.find_by_email('boss@mail.com').delete
    User.find_by_email('dev@mail.com').delete
    User.find_by_email('hr@mail.com').delete
    User.find_by_email('manager@mail.com').delete
    User.find_by_email('account_manager@mail.com').delete
    User.find_by_email('moderator@mail.com').delete
  end
end

