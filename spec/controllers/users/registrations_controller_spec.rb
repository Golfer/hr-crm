require 'rails_helper'

describe Users::RegistrationsController, type: :controller do
  before do
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  describe '#create' do
    subject! { post :create, params: { user: attributes_for(:user, email: 'user_123@mail.com') }}
    it { expect(subject).to redirect_to root_path }

    context "#invalid registration params" do
      subject! { post :create, params: { user: attributes_for(:user, password: '', email: 'user_123@mail.com') }}
      it { expect(response).to render_template :new }
    end
  end
end
