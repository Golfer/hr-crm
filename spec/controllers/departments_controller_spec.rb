require 'rails_helper'

describe DepartmentsController, type: :request do
  let!(:departments) { FactoryGirl.create_list(:department, 23) }
  let(:curr_department) { FactoryGirl.create(:department, name: 'TestDepName') }
  let(:department_with_employee) { FactoryGirl.create(:department_with_employee) }
  let(:user_for_sign_in) { FactoryGirl.create(:user) }
  describe '#index' do
    before(:each) do
      get departments_path
    end
    it { expect(response).to have_http_status(:success) }
    it { expect(response).to render_template :index }
    it { expect(assigns(:departments).count).to eq 23 }
  end

  describe '#show' do
    before { sign_in(user_for_sign_in) }
    context 'send Wrong ID' do
      before(:each) do
        get department_path(id: 123)
      end
      it { expect(response.code).to eq '422' }
      it { expect(response).to render_template :index }
      it { expect(response).to render_template(partial: '_flash_messages') }
    end
    context 'render show' do
      before(:each) do
        get department_path(curr_department)
      end
      it { expect(response.code).to eq '200' }
      it { expect(assigns(:department)[:name]).to eq curr_department[:name] }
    end
  end

  describe '#create' do
    before { sign_in(user_for_sign_in) }
    subject { post departments_path(department: attributes_for(:department, name: 'New Department123')) }
    it { expect(subject).to redirect_to(departments_path) }

    context 'error. Department exist' do
      before(:each) do
        create(:department, name: 'New Department123')
        post api_default_departments_path(department: attributes_for(:department, name: 'New Department123'))
      end
      it { expect(response.code).to eq '422' }
      it { expect(response.body).to include('errors') }
    end
  end

  describe '#update' do
    before { sign_in(user_for_sign_in) }
    subject { patch department_path(curr_department, department: { name: 'DepartmentNameUpdated' }) }
    it { expect(subject).to eq 302 }
    it { expect(subject).to redirect_to action: :show }
    context 'validation error' do
      before(:each) do
        patch department_path(curr_department, department: { name: 'A' })
      end
      it { expect(response).to render_template :edit }
      it { expect(response).to render_template(partial: '_flash_messages') }
    end
  end

  describe '#destroy' do
    before(:each) do
      sign_in(user_for_sign_in)
      delete department_path(curr_department)
    end
    it { expect(subject).to redirect_to departments_path }
  end
  describe '#connect_employees' do
  end
end
