require 'rails_helper'

describe Api::SessionsController, type: :request do
  let(:json_body) { JSON.parse(response.body) }
  let!(:user_for_sign_in) { FactoryGirl.create(:user, email: 'employee_test_1@mail.com', password: '123123123') }

  describe '#create' do
    before(:each) do
      post api_sessions_path(user: {email: user_for_sign_in.email, auth_password: user_for_sign_in.password})
    end
    it { expect(response).to have_http_status(:success) }
    it {expect(response.body).to match user_for_sign_in.email }
    it {expect(response.body).to match 'authentication_token' }
  end

  describe '#destroy' do
    before(:each) do
      post api_sessions_path(user: {email: user_for_sign_in.email, auth_password: user_for_sign_in.password})
      delete destroy_user_session_path(user_for_sign_in)
    end
    it { expect(response.code).to eq('204') }
  end


  describe '#current' do
    before(:each) do
      post api_sessions_path(user: {email: user_for_sign_in.email, auth_password: user_for_sign_in.password})
      get current_api_sessions_path
    end
    it { expect(response).to have_http_status(:success) }
    it {expect(response.body).to match user_for_sign_in.email }
    it {expect(response.body).to match 'authentication_token' }
  end
end
