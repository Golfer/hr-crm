require 'rails_helper'

describe Api::EmployeesController, type: :request do
  let(:json_body) { JSON.parse(response.body) }
  let(:employees) { json_body['employees'] }
  let(:pagination) { json_body['meta']['pagination'] }
  let(:links) { json_body['meta']['links'] }
  let!(:curr_employee) { FactoryGirl.create(:employee_with_profile) }
  let!(:curr_employee_with_schedule) { FactoryGirl.create(:employee_with_profile_and_schedule) }
  let!(:curr_employee_without_profile) { FactoryGirl.create(:employee) }
  let(:user_for_sign_in) { FactoryGirl.create(:user) }

  describe '#index' do
    before(:each) do
      23.times { FactoryGirl.create(:employee_with_profile) }
      get api_default_employees_path
    end
    it { expect(response.content_type).to eq 'application/json' }
    it { expect(response.code).to eq('200') }
    it { expect(response).to have_http_status(:success) }
    it { expect(employees.count).to eq 5 }
    it { expect(pagination).to_not be_nil }
    it { expect(links).to_not be_nil }
    it { expect(pagination['current_page']).to eq 1 }
    it { expect(pagination['per_page']).to_not be_nil }
    it { expect(pagination['total_pages']).to_not be_nil }
    it { expect(pagination['total_entries']).to_not be_nil }

    it 'with page params' do
      get api_default_employees_path(page: 2)
      expect(pagination['current_page']).to eq 2
    end
  end

  describe '#show' do
    before { sign_in(user_for_sign_in) }
    context 'render Error. Send wrong ID' do
      before(:each) do
        get api_default_employee_path(id: 123)
      end
      it { expect(response.code).to eq('404') }
      it { expect(json_body.keys).to include('error') }
    end

    context 'with_out prifile' do
      before(:each) do
        get api_v1_employee_path(curr_employee_without_profile)
      end
      it { expect(response.code).to eq('200') }
      it { expect(response).to have_http_status(:success) }
      it { expect(json_body.keys).to_not include('error') }
      it { expect(json_body['profile']).to eq nil }
    end

    context 'with profile & blank working_hours' do
      before(:each) do
        get api_v1_employee_path(curr_employee)
      end
      it { expect(response.code).to eq('200') }
      it { expect(response).to have_http_status(:success) }
      it { expect(json_body['working_hours']).to eq nil }
    end

    context 'with profile & not blank working_hours' do
      before(:each) do
        get api_v1_employee_path(curr_employee_with_schedule)
      end
      it { expect(response.code).to eq('200') }
      it { expect(response).to have_http_status(:success) }
      it { expect(json_body['profile']['working_hours']).to_not eq nil }
    end
  end

  describe '#create' do
    before(:each) do
      sign_in(user_for_sign_in)
      post api_default_employees_path(employee: attributes_for(:employee, email: 'employee_test_1@mail.com', profile: attributes_for(:profile)))
    end
    it { expect(response.code).to eq('201') }
    it { expect(response).to have_http_status(:success) }
    it { expect(json_body['email']).to eq 'employee_test_1@mail.com' }

    context 'error. Employee with email alreacy exist' do
      before(:each) do
        post api_default_employees_path(employee: attributes_for(:employee, email: 'employee_test_1@mail.com', profile: attributes_for(:profile)))
      end
      it { expect(response.code).to eq '422' }
      it { expect(response).to have_http_status(:unprocessable_entity) }
      it { expect(json_body['employee'].first).to eql('Email has already been taken') }
    end
  end

  describe '#update' do
    before(:each) do
      sign_in(user_for_sign_in)
      patch api_default_employee_path(curr_employee, employee: { email: 'New_valid_mail@mail.com' })
    end

    it { expect(response.code).to eq('200') }
    it { expect(json_body['email']).to eq 'New_valid_mail@mail.com' }

    context 'validation error' do
      before(:each) do
        put api_default_employee_path(curr_employee, employee: { email: 'Invalid_mail' })
      end
      it { expect(response.code).to eq '422' }
      it { expect(json_body['email']).to include('is invalid') }
    end

    context 'profile invalid' do
      before(:each) do
        patch api_default_employee_path(curr_employee, employee: { profile: { first_name: 'FN', date_birth: '' } })
      end
      it { expect(response.code).to eq('422') }
      it { expect(json_body['date_birth'].first).to eq 'can\'t be blank' }
    end

    context 'profile valid' do
      before(:each) do
        patch api_default_employee_path(curr_employee, employee: { profile: { first_name: 'FName_New', date_birth: '12-12-2012' } })
      end
      it { expect(response.code).to eq('200') }
      it { expect(json_body['profile']['first_name']).to eq 'FName_New' }
      it { expect(json_body['profile']['date_birth']).to eq '12-12-2012' }
    end
  end

  describe '#destroy' do
    before { sign_in(user_for_sign_in) }
    context 'success' do
      before(:each) do
        delete api_default_employee_path(curr_employee)
      end
      it { expect(response.code).to eq('204') }
    end
    context 'unsuccess' do
      before(:each) do
        delete api_default_employee_path(id: 123)
      end
      it { expect(response.code).to eq('404') }
      it { expect(json_body.keys).to include('error') }
    end
  end
end
