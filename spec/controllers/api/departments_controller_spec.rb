require 'rails_helper'

describe Api::DepartmentsController, type: :request do
  let(:json_body) { JSON.parse(response.body) }
  let(:departments) { json_body['departments'] }
  let(:pagination) { json_body['meta']['pagination'] }
  let(:links) { json_body['meta']['links'] }
  let!(:curr_department) { FactoryGirl.create(:department, name: 'TestDepName') }
  let!(:department_with_employee) { FactoryGirl.create(:department_with_employee) }
  let(:user_for_sign_in) { FactoryGirl.create(:user) }

  describe '#index' do
    before(:each) do
      FactoryGirl.create(:department, name: 'ADepName')
      FactoryGirl.create(:department, name: 'ZDepName')
      23.times { |n| FactoryGirl.create(:department, name: "Dep-Name-#{n}") }
      get api_default_departments_path
    end
    it { expect(response.content_type).to eq 'application/json' }
    it { expect(response.code).to eq('200') }
    it { expect(response).to have_http_status(:success) }
    it { expect(departments.count).to eq 5 }
    it { expect(pagination).to_not be_nil }
    it { expect(links).to_not be_nil }
    it { expect(pagination['current_page']).to eq 1 }
    it { expect(pagination['per_page']).to_not be_nil }
    it { expect(pagination['total_pages']).to_not be_nil }
    it { expect(pagination['total_entries']).to_not be_nil }

    it 'with sort params Z -> A' do
      get api_default_departments_path(sort: '-name')
      expect(json_body['departments'].first['name']).to eq 'ZDepName'
    end
    it 'with sort params A -> Z' do
      get api_default_departments_path(sort: 'name')
      expect(json_body['departments'].first['name']).to eq 'ADepName'
    end
    it 'with page params' do
      get api_default_departments_path(page: 2)
      expect(pagination['current_page']).to eq 2
    end
  end

  describe '#show' do
    before { sign_in(user_for_sign_in) }
    context 'render Error. Send wrong ID' do
      before(:each) do
        get api_default_department_path(id: 123)
      end
      it { expect(response.code).to eq('404') }
      it { expect(json_body.keys).to include('error') }
    end

    context 'with_out employees' do
      before(:each) do
        get api_v1_department_path(curr_department)
      end
      it { expect(response.code).to eq('200') }
      it { expect(response).to have_http_status(:success) }
      it { expect(json_body.keys).to_not include('error') }
      it { expect(json_body['name']).to eq 'TestDepName' }
      it { expect(json_body['count_of_employees']).to eq 0 }
      it { expect(json_body['employees']).to eq [] }
    end

    context 'with employees' do
      before(:each) do
        get api_v1_department_path(department_with_employee)
      end
      it { expect(response.code).to eq('200') }
      it { expect(response).to have_http_status(:success) }
      it { expect(json_body['count_of_employees']).to eq 1 }
      it { expect(json_body['employees']).to_not eq [] }
    end
  end

  describe '#create' do
    before(:each) do
      sign_in(user_for_sign_in)
      post api_default_departments_path(department: attributes_for(:department, name: 'New Department123'))
    end
    it { expect(response.code).to eq('201') }
    it { expect(response).to have_http_status(:success) }
    it { expect(json_body['name']).to eq 'New Department123' }

    context 'error. Department exist' do
      before(:each) do
        post api_default_departments_path(department: attributes_for(:department, name: 'New Department123'))
      end
      it { expect(response.code).to eq '422' }
      it { expect(response).to have_http_status(:unprocessable_entity) }
      it { expect(json_body.keys).to include('errors') }
    end
  end

  describe '#update' do
    before(:each) do
      sign_in(user_for_sign_in)
      patch api_default_department_path(curr_department, department: { name: 'DepartmentNameUpdated' })
    end
    it { expect(response.code).to eq('200') }
    it { expect(json_body['name']).to eq 'DepartmentNameUpdated' }

    context 'validation error' do
      before(:each) do
        put api_default_department_path(curr_department, department: { name: 'A' })
      end
      it { expect(response.code).to eq '422' }
      it { expect(json_body.keys).to include('name') }
    end
  end

  describe '#destroy' do
    context 'success' do
      before(:each) do
        sign_in(user_for_sign_in)
        delete api_default_department_path(curr_department)
      end
      it { expect(response.code).to eq('204') }
    end
    context 'unsuccess' do
      before(:each) do
        delete api_default_department_path(id: 123)
      end
      it { expect(response.code).to eq('404') }
      it { expect(json_body.keys).to include('error') }
    end
  end

  describe '#connect_employees' do
    before(:each) do
      sign_in(user_for_sign_in)
      employee1 = FactoryGirl.create(:employee)
      employee2 = FactoryGirl.create(:employee)
      employee3 = FactoryGirl.create(:employee)
      post connect_employees_api_default_department_path(curr_department),
        params: {
          department: {
            employee: [
              {
                email: employee1.email
              },
              {
                email: employee2.email
              },
              {
                email: employee3.email
              }
            ]
          }
        }
    end
    it { expect(response.code).to eq('200') }
    it { expect(response.content_type).to eq 'application/json' }
    it { expect(json_body['count_of_employees']).to eq 3 }
  end

  describe '#disconnect_employees' do
    before(:each) do
      sign_in(user_for_sign_in)
      post disconnect_employees_api_default_department_path(department_with_employee),
           params: { department: { employee: [{ email: department_with_employee.employees.first.email }] } }
    end
    it { expect(response.code).to eq('200') }
    it { expect(response.content_type).to eq 'application/json' }
    it { expect(json_body['count_of_employees']).to eq 0 }
  end
end
