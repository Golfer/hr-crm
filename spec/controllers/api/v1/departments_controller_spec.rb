require 'rails_helper'

describe Api::V1::DepartmentsController, type: :request do
  let(:json_body) { JSON.parse(response.body) }
  describe '#index' do
    before(:each) do
      FactoryGirl.create(:department, name: 'A-Dep-Name')
      FactoryGirl.create(:department, name: 'Z-Dep-Name')
      23.times { |n| FactoryGirl.create(:department, name: "Dep-Name-#{n}") }
      get api_v1_departments_path
    end
    it { expect(response.content_type).to eq 'application/json' }
    it { expect(response.code).to eq('200') }
    it { expect(response).to have_http_status(:success) }
    it { expect(json_body['departments'].first.keys).to include('name', 'description', 'count_of_employees', 'employees') }
    it 'include first_page to links block' do
      get api_v1_departments_path(page: 3)
      expect(json_body['meta']['links']['first_page']).to_not be_nil
    end

    it 'include last_page to links block' do
      expect(json_body['meta']['links']['last_page']).to_not be_nil
    end
  end
end
