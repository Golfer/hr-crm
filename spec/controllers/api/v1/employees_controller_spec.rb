require 'rails_helper'

describe Api::V1::EmployeesController, type: :request do
  let(:json_body) { JSON.parse(response.body) }
  subject! { FactoryGirl.create_list(:employee_with_profile, 23) }
  describe '#index' do
    before(:each) do
      FactoryGirl.create(:employee, profile: FactoryGirl.create(:profile, first_name: 'A_Employee_First_name_A'))
      FactoryGirl.create(:employee, profile: FactoryGirl.create(:profile, first_name: 'Z_Employee_First_name_Z'))
      get api_v1_employees_path
    end
    it { expect(response.content_type).to eq 'application/json' }
    it { expect(response.code).to eq('200') }
    it { expect(response).to have_http_status(:success) }
    it { expect(json_body['employees'].first.keys).to include('profile', 'email', 'in_vocation') }
    it 'include first_page to links block' do
      get api_v1_employees_path(page: 3)
      expect(json_body['meta']['links']['first_page']).to_not be_nil
    end

    it 'include last_page to links block' do
      expect(json_body['meta']['links']['last_page']).to_not be_nil
    end
  end
end
