require 'rails_helper'

describe Api::V2::DepartmentsController, type: :request do
  let(:json_body) { JSON.parse(response.body) }
  describe '#index' do
    before(:each) do
      FactoryGirl.create(:department, name: 'ADepName')
      FactoryGirl.create(:department, name: 'ZDepName')
      23.times { |n| FactoryGirl.create(:department, name: "Dep-Name-#{n}") }
      get api_v2_departments_path
    end
    it { expect(response.content_type).to eq 'application/json' }
    it { expect(response.code).to eq('200') }
    it { expect(response).to have_http_status(:success) }
    it { expect(json_body['departments'].first.keys).to include('name', 'id', 'count_of_employees', 'employees') }
    it { expect(json_body['departments'].first['name']).to eq 'ADepName' }
    it 'not include first_page to links block' do
      expect(json_body['meta']['links']['first_page']).to be_nil
    end
    it 'not include last_page to links block' do
      expect(json_body['meta']['links']['last_page']).to be_nil
    end
  end
end
