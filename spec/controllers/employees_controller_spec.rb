require 'rails_helper'

describe EmployeesController, type: :request do
  let(:user_for_sign_in) { FactoryGirl.create(:user) }
  let(:employee_with_schedule) { FactoryGirl.create(:employee_with_profile_and_schedule) }
  let!(:employees) { FactoryGirl.create_list(:employee_with_profile, 15) }
  let(:curr_employee) { FactoryGirl.create(:employee_with_profile_and_schedule) }
  let(:curr_employee_2) { FactoryGirl.create(:employee_with_profile) }

  describe '#index' do
    before(:each) do
      sign_in(user_for_sign_in)
      employee_with_schedule
      get employees_path
    end
    it { expect(response).to have_http_status(:success) }
    it { expect(response).to render_template :index }
    it { expect(assigns(:employees).count).to eq 16 }
  end

  describe '#show' do
    before { sign_in(user_for_sign_in) }
    context 'send Wrong ID' do
      before(:each) do
        get employee_path(id: 123)
      end
      it { expect(response.code).to eq '422' }
      it { expect(response).to render_template :index }
      it { expect(response).to render_template(partial: '_flash_messages') }
    end
    context 'render show' do
      before(:each) do
        get employee_path(curr_employee)
      end
      it { expect(response.code).to eq '200' }
      it { expect(assigns(:employee)[:email]).to eq curr_employee[:email] }
    end

    context 'with workingHours' do
      before(:each) do
        get employee_path(curr_employee_2)
      end
      it { expect(response.code).to eq '200' }
      it { expect(response).to render_template 'employees/_working_hours' }
    end
  end

  describe '#create' do
    before { sign_in(user_for_sign_in) }
    subject! { post employees_path(employee: attributes_for(:employee_with_profile, email: 'employee_123@mail.com')) }
    it { expect(response).to render_template :new }
    it { expect(response).to have_http_status(:success) }
    it { expect(response).to render_template(partial: '_flash_messages') }

    context 'error. Employee exist' do
      before(:each) do
        create(:employee_with_profile, email: 'employee_123@mail.com')
        post employees_path(employee: attributes_for(:employee_with_profile, email: 'employee_123@mail.com'))
      end
      it { expect(response).to render_template(partial: '_flash_messages') }
      it { expect(response).to have_http_status(:success) }
      it { expect(response.body).to include('errors') }
    end
  end

  describe '#update' do
    before { sign_in(user_for_sign_in) }
    subject { patch employee_path(curr_employee, employee: { email: 'employee_123@mail.com' }) }
  end

  describe '#destroy' do
    before { sign_in(user_for_sign_in) }

    before(:each) do
      delete employee_path(curr_employee)
    end
    it { expect(subject).to redirect_to employees_path }
  end
  describe '#related_to_department'
end
