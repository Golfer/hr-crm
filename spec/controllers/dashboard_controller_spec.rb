require 'rails_helper'

describe DashboardController, type: :request do
  let!(:departments) do
    employee1 = FactoryGirl.create(
      :employee, email: 'test123@mail.com',
        profile: create(:profile_with_schedule, first_name: 'Just_for_test123')
    )
    department = FactoryGirl.create(:department_with_employee, name: 'Department_1')
    department.employees << employee1
    FactoryGirl.create_list(:department_with_employee, 6)
  end

  subject { 3.times { FactoryGirl.create(:department_with_employee) } }
  describe '#index' do
    before(:each) { get root_path }
    it { expect(response).to have_http_status(:success) }
    it { expect(response).to render_template :index }
    it { expect(assigns(:departments).count).to eq 7 }
    it { expect(assigns(:departments).first.name).to eql 'Department_1' }
    it { expect(assigns(:departments).first.employees.count).to eql 2 }
    it { expect(assigns(:departments).first.employees.first.profile.first_name).to eql 'Just_for_test123' }
  end
end
