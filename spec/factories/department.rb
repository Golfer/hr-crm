FactoryGirl.define do
  factory :department do
    sequence(:name) { |n| "#{FFaker::Company.name}-#{n}" }
    description FFaker::Lorem.words
  end

  factory :department_with_employee, parent: :department do
    after(:create) do |department|
      department.employees << FactoryGirl.create(:employee_with_profile)
    end
  end

  factory :department_with_employee_and_schedule, parent: :department do
    after(:create) do |department|
      department.employees << FactoryGirl.create(:employee_with_profile_and_schedule)
    end
  end
end
