FactoryGirl.define do
  factory :profile do
    first_name FFaker::Name.first_name
    second_name FFaker::Name.name_with_prefix
    last_name FFaker::Name.last_name
    phone_number FFaker::PhoneNumber.phone_number
    date_birth DateTime.now.strftime('%Y-%m-%d %H-%M')
    gender FFaker::Gender.random
  end

  factory :profile_with_schedule, parent: :profile do
    working_hours {{
      monday: {
        from: '10:00 AM',
        to: '4:00 PM'
      },
      tuesday: {
        from: '11:00 AM',
        to: '2:00 PM'
      },
      wednesday: {
        from: '9:00 AM',
        to: '1:00 PM'
      },
      thursday: {
        from: '8:00 AM',
        to: '5:00 PM'
      },
      friday: {
        from: '10:00 AM',
        to: '1:00 PM'
      },
      saturday: {
        from: '11:00 AM',
        to: '1:00 PM'
      }
    }}
  end
end
