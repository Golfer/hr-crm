FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "#{FFaker::Internet.email}-#{n}" }
    password FFaker::Internet.password
  end

  factory :dev_user, parent: :user do
    after(:build) do |user|
      user.roles << Role.find_or_create_by(name: 'ROLE_DEV')
    end
  end

  factory :hr_user, parent: :user do
    after(:build) do |user|
      user.roles << Role.find_or_create_by(name: 'ROLE_HR')
    end
  end

  factory :boss_user, parent: :user do
    after(:build) do |user|
      user.roles << Role.find_or_create_by(name: 'ROLE_BOSS')
    end
  end

  factory :manager_user, parent: :user do
    after(:build) do |user|
      user.roles << Role.find_or_create_by(name: 'ROLE_MANAGER')
    end
  end

  factory :account_manager_user, parent: :user do
    after(:build) do |user|
      user.roles << Role.find_or_create_by(name: 'ROLE_ACCOUNT_MANAGER')
    end
  end

  factory :moderator_user, parent: :user do
    after(:build) do |user|
      user.roles << Role.find_or_create_by(name: 'ROLE_MODERATOR')
    end
  end

  factory :employee_user, parent: :user do
    after(:build) do |user|
      user.roles << Role.find_or_create_by(name: 'ROLE_EMPLOYEE')
    end
  end

  factory :user_with_profile, parent: :user do
    association :profile, factory: :profile
  end
end
