FactoryGirl.define do
  factory :role do
    sequence(:name) { |n| "ROLE_#{FFaker::Name.name}_#{n}" }
  end

  factory :role_boss, parent: :role do
    name 'ROLE_BOSS'
  end

  factory :role_hr, parent: :role do
    name 'ROLE_HR'
  end

  factory :role_dev, parent: :role do
    name 'ROLE_DEV'
  end

  factory :role_manager, parent: :role do
    name 'ROLE_MANAGER'
  end

  factory :role_account_manager, parent: :role do
    name 'ROLE_ACCOUNT_MANAGER'
  end

  factory :role_moderator, parent: :role do
    name 'ROLE_MODERATOR'
  end

  factory :role_employee, parent: :role do
    name 'ROLE_EMPLOYEE'
  end
end
