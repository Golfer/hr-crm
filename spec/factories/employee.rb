FactoryGirl.define do
  factory :employee do
    sequence(:email) { |n| "#{FFaker::Internet.free_email}_#{n}" }
  end

  factory :employee_with_profile, parent: :employee do
    association :profile, factory: :profile
  end

  factory :employee_with_profile_and_schedule, parent: :employee do
    association :profile, factory: :profile_with_schedule
  end
end
