require 'rails_helper'

describe RolesUser, type: :model do
  subject { FactoryGirl.create(:user, role: FactoryGirl.create(:role_dev)) }

  describe 'relations' do
    it { expect(RolesUser.reflect_on_association(:user).macro).to eq(:belongs_to) }
    it { expect(RolesUser.reflect_on_association(:role).macro).to eq(:belongs_to) }
  end
end
