require 'rails_helper'

describe Profile, type: :model do
  subject { FactoryGirl.create(:profile) }

  describe 'relations' do
    it { expect(Profile.reflect_on_association(:user).macro).to eq(:belongs_to) }
    it { expect(Profile.reflect_on_association(:employee).macro).to eq(:belongs_to) }
  end

  describe 'validates' do
    it { expect(subject).to validate_presence_of(:first_name) }
    it { expect(subject).to validate_presence_of(:last_name) }
    it { expect(subject).to validate_presence_of(:gender) }
    it { expect(subject).to validate_presence_of(:date_birth) }
  end
end
