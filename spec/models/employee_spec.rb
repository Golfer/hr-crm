require 'rails_helper'

describe Employee, type: :model do
  subject { FactoryGirl.create(:employee_with_profile) }

  describe 'relations' do
    it { expect(Employee.reflect_on_association(:departments).macro).to eq(:has_and_belongs_to_many) }
    it { expect(Employee.reflect_on_association(:profile).macro).to eq(:has_one) }
  end

  describe 'validates' do
    it { expect(subject).to validate_presence_of(:email) }
    it { expect(subject).to validate_uniqueness_of(:email) }
  end
end
