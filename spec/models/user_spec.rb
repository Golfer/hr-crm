require 'rails_helper'

describe User, type: :model do
  subject { FactoryGirl.create(:user_with_profile) }

  describe 'relations' do
    it { expect(User.reflect_on_association(:roles).macro).to eq(:has_and_belongs_to_many) }
    it { expect(User.reflect_on_association(:profile).macro).to eq(:has_one) }
  end
end
