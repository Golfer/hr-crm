require 'rails_helper'

describe Department, type: :model do
  subject { FactoryGirl.create(:department) }
  let(:department_with_employees) { FactoryGirl.create(:department_with_employee) }

  describe 'relations' do
    it { expect(subject).to have_and_belong_to_many(:employees) }
  end

  describe 'validates' do
    it { expect(subject).to validate_presence_of(:name) }
    it { expect(subject).to validate_uniqueness_of(:name) }
  end

  describe '#count_of_employees' do
    before do
      5.times { department_with_employees.employees << FactoryGirl.create(:employee_with_profile) }
    end
    it { expect(department_with_employees.employees.count).to eql 6 }
  end
end
