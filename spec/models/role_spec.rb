require 'rails_helper'

describe Role, type: :model do
  subject { FactoryGirl.create(:role) }

  describe 'relations' do
    it { expect(subject).to have_and_belong_to_many(:users) }
  end

  describe 'validates' do
    it { expect(subject).to validate_presence_of(:name) }
    it { expect(subject).to validate_uniqueness_of(:name) }
    it { expect(subject).to validate_length_of(:name).is_at_most(32) }
  end
end
