require 'rails_helper'

describe EmployeePolicy do
  subject! { described_class.new(user, employee) }
  let(:employee) { FactoryGirl.create(:employee_with_profile) }

  context 'when Quest' do
    let(:user) { nil }
    it { is_expected.to permit_action(:index) }
    it { is_expected.to forbid_actions(%I[new create edit update destroy related_to_department]) }
  end

  context 'when BOSS logined' do
    let(:user) { FactoryGirl.create(:boss_user) }
    it { is_expected.to permit_actions(%I[index destroy new update related_to_department]) }
    it { is_expected.to forbid_action(:create_employee_account) }
  end

  context 'when HR loggined' do
    let(:user) { FactoryGirl.create(:hr_user) }
    it { is_expected.to permit_actions(%I[index destroy new update related_to_department create_employee_account]) }
  end

  context 'when Moderator logined' do
    let(:user) { FactoryGirl.create(:moderator_user) }
    it { is_expected.to permit_actions(%I[index destroy new update related_to_department create_employee_account]) }
  end
end
