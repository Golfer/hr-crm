require 'rails_helper'

describe DepartmentPolicy do
  subject! { described_class.new(user, department) }
  let(:department) { FactoryGirl.create(:department) }

  context 'when Quest' do
    let(:user) { nil }
    it { is_expected.to permit_action(:index) }
    it { is_expected.to forbid_actions(%I[create update destroy connect_to_department connect_employees]) }
  end

  context 'when BOSS logined' do
    let(:user) { FactoryGirl.create(:boss_user) }
    it { is_expected.to permit_actions(%I[index destroy new update connect_employees]) }
    it { is_expected.to forbid_action(:connect_to_department) }
  end

  context 'when HR loggined' do
    let(:user) { FactoryGirl.create(:hr_user) }
    it { is_expected.to permit_actions(%I[index destroy new update connect_employees connect_to_department]) }
  end

  context 'when Moderator logined' do
    let(:user) { FactoryGirl.create(:moderator_user) }
    it { is_expected.to permit_actions(%I[index destroy new update connect_employees]) }
  end

  context 'when Employee logined' do
    let(:user) { FactoryGirl.create(:employee_user) }
    it { is_expected.to permit_actions(%I[index connect_to_department]) }
    it { is_expected.to forbid_actions(%I[destroy new create update connect_employees]) }
  end

  context 'when Manager logined' do
    let(:user) { FactoryGirl.create(:manager_user) }
    it { is_expected.to permit_action(:index) }
    it { is_expected.to forbid_actions(%I[new create update destroy connect_employees connect_to_department]) }
  end

  context 'when Account Manager logined' do
    let(:user) { FactoryGirl.create(:account_manager_user) }
    it { is_expected.to permit_action(:index) }
    it { is_expected.to forbid_actions(%I[new create update destroy connect_employees connect_to_department]) }
  end
end
