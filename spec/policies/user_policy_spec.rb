require 'rails_helper'

describe UserPolicy do
  subject! { described_class.new(user, user_record) }
  let(:user_record) { FactoryGirl.create(:employee_user) }

  context 'when Quest' do
    let(:user) { nil }
    it { is_expected.to forbid_actions(%I[index create update destroy]) }
  end

  context 'when User logined & Employer withOut Employee account' do
    let(:user) { user_record }
    it { is_expected.to permit_action(:create_employee_account) }
  end
end
