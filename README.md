Link to HEROKU DEMO 
````
https://hr-crm-testapp.herokuapp.com/
````

API USAGE DOCUMENT
 
Basic logic for API Authorization

````
http://localhost:3000/api/sessions?email=[EMAIL]&auth_password=[PASSWORD]
````

Content Type :
   ````
   application/json
   ````
Body:
  You can pass  json data in Body
  ````
    sample json body
    
    {
    "department": {
        "name": "TestNameDepartment",
        "description": "Test Description for department"
      }
    }    
  ````
  
For get list of [Departments|Employees]

````
GET request
http://localhost:3000/api/[departments|employees]
````
Sort department default by ASC 

Set order by [desc|asc]

````
http://localhost:5003/api/[departments|employees]?sort=[-/+][sort_field]
````

Show record

````
http://localhost:5003/api/[departments/employees]/[ID]
````
Create for example [Department] | [Employee]

````
POST request
http://localhost:3000/api/departments

{
"department": {
    "name": "TestNameDepartment",
    "description": "Test Description for department"
  }
}
````

Update for example [Department]

````
PATCH request
http://localhost:3000/api/departments/[ID]


{
	"department":{
		"description":"Description Update"
	} 
}
````


DELETE record. For example [Department]
 
```` 
http://localhost:3000/api/departments/[ID]
````


Connect employee to department

````
department[employee][][email] = 'test@mail.com'
````
OR  
````
{
	"department": {
	"id": "20",
	"employee": [{
		"email": "123123123"
	},
	{
		"email":"321321321"
	}]
	}
}
````


Description Application

Приложение для работы управления людьми и департаментами компании. 
Приложение состоит из 2х частей: API endpoints для мобильного приложения и несколько HTML страниц для
демонстрации на Веб. 

API endpoints

````
1. Пользователь может аутентифицироваться
2. Пользователь может создать новый департамент компании (название*, описание).
3. Пользователь может создать сотрудника (имя*, год рождения*, пол*, должность*).
4. Пользователь может добавлять в департамент сотрудников
5. Пользователь может просмотреть все созданные департаменты
6. Пользователь может просмотреть всех сотрудников определенного департамента (наличие пагинации)
7. Пользователь может сортировать список департаментов (A-Z)
8. Пользователь может сортировать списки сотрудников по имени (A-Z), по году рождения, полу
9. Пользователь может редактировать/удалять сотрудника
10. Пользователь может редактировать/удалять департамент
````

Frontend страницы

````
1. Страницы аутентификации пользователя
2. Страница с перечнем всех департаментов
3. Страница департамента со списком сотрудников
4. Страница создание/редактирование департамента
5. Страница создание/редактирование сотрудника
6. Возможность удалить сотрудника
7. На странице всех департаментов отобразить график сотрудников в департаменте, где по X - департамент,
а по Y - количество сотрудников.
* обязательные поля
````

Технологии

````
● Devise
● Pundit
● Twitter Bootstrap
● SLIM template engine
● SASS
● PostgreSQL
● RSpec
````
